﻿using System;
using System.IO;
using MindWorX.Unmanaged;
using Serilog;
using Serilog.Events;
using SharpCraft.Framework;

namespace SharpCraft.TroubleShooter
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var environment = new SharpCraftEnvironment("SharpCraft.TroubleShooter", Environment.CurrentDirectory);
            environment.LoadProfiles();

            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Verbose()
                .WriteTo.LiterateConsole(LogEventLevel.Information)
                .WriteTo.RollingFile(Path.Combine(environment.LogsDirectory, @"diagnostics.txt"), retainedFileCountLimit: 1)
                .CreateLogger();

            Console.WriteLine("This troubleshooter is highly experimental and far from finished.");
            Console.WriteLine("The troubleshooter will show you obvious errors it catches, but also generate a long log named diagonostics in the logs folder.");
            Console.Write("Press any key to continue . . .");
            Console.ReadKey(true);
            Console.WriteLine();
            Console.WriteLine();

            foreach (var file in Directory.EnumerateFiles(Environment.CurrentDirectory, "*.*", SearchOption.AllDirectories))
            {
                var zone = FileEx.GetSecurityZone(file);
                if ((int)zone > 2)
                {
                    if (!FileEx.RemoveSecurityZone(file))
                        Log.Logger.Error("Unabled to unblock file: {@file}", new { File = file, Zone = zone });
                    else
                        Log.Logger.Warning("Unblocked file: {@file}", new { File = file, Zone = zone });
                }
                else
                {
                    Log.Logger.Verbose("{@file}", new { File = file, Zone = zone });
                }
            }

            Console.WriteLine();
            Console.WriteLine("Done!");
            Console.WriteLine("If you need to report this, please upload a copy of the diagnostics log.");
            Console.Write("Press any key to continue . . .");
            Console.ReadKey(true);
        }
    }
}
