﻿using System.ComponentModel.Composition;

namespace SharpCraft.Framework
{
    public delegate void PluginReadyEventHandler(object sender);

    [InheritedExport(typeof(IPlugin))]
    public interface IPlugin
    {
        event PluginReadyEventHandler Ready;

        IPluginInfo Info { get; }

        bool IsReady { get; }

        void Initialize();
    }
}
