﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace SharpCraft.Framework
{
    [XmlRoot]
    [Serializable]
    public class Profile
    {
        public static Profile Load(string root)
        {
            var profilePath = Path.Combine(root, "profile.xml");

            if (!File.Exists(profilePath))
                throw new FileNotFoundException("'profile.xml' missing.");

            var serializer = new XmlSerializer(typeof(Profile));
            using (var stream = File.Open(profilePath, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                var profile = (Profile)serializer.Deserialize(stream);

                profile.Name = Path.GetFileName(root);
                profile.Root = root;

                if (!Directory.Exists(profile.Root))
                    Directory.CreateDirectory(profile.Root);
                if (!Directory.Exists(profile.PluginsDirectory))
                    Directory.CreateDirectory(profile.PluginsDirectory);

                return profile;
            }
        }


        public Profile() { }

        public Profile(string root, ProfileType type)
        {
            if (String.IsNullOrEmpty(root))
                throw new ArgumentNullException(nameof(root));
            this.Name = Path.GetFileName(root);
            this.Root = root;
            this.Type = type;

            if (!Directory.Exists(this.Root))
                Directory.CreateDirectory(this.Root);
            if (!Directory.Exists(this.PluginsDirectory))
                Directory.CreateDirectory(this.PluginsDirectory);
        }

        [XmlIgnore]
        public string Name { get; private set; }

        [XmlIgnore]
        public string Root { get; private set; }

        [XmlIgnore]
        public string PluginsDirectory => Path.Combine(this.Root, "plugins");

        [XmlAttribute(nameof(Type))]
        public string TypeValue { get; set; }

        [XmlIgnore]
        public ProfileType Type
        {
            get { return new ProfileType(this.TypeValue); }
            set { this.TypeValue = value.Name; }
        }

        [XmlAttribute]
        public bool EnableSharpCraft { get; set; } = false;

        [XmlAttribute]
        public bool EnableDebug { get; set; } = false;

        [XmlAttribute]
        public bool EnableDiagnosticsDebug { get; set; } = false;

        [XmlElement]
        public string FileName { get; set; } = String.Empty;

        [XmlElement]
        public string Arguments { get; set; } = String.Empty;

        [XmlElement]
        public string WorkingDirectory { get; set; } = String.Empty;

        public void Save()
        {
            var serializer = new XmlSerializer(typeof(Profile));
            using (var stream = File.Open(Path.Combine(this.Root, "profile.xml"), FileMode.Create, FileAccess.Write, FileShare.Write))
                serializer.Serialize(stream, this);
        }
    }
}
