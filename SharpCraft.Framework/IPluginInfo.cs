﻿using System.Collections.Generic;

namespace SharpCraft.Framework
{
    public interface IPluginInfo
    {
        string Name { get; }

        string Description { get;  }

        string Version { get; }

        IEnumerable<string> Authors { get; }
    }
}