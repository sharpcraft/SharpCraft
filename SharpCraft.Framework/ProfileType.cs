﻿using System;

namespace SharpCraft.Framework
{
    public struct ProfileType : IEquatable<ProfileType>
    {
        public static ProfileType SharpCraft { get; } = new ProfileType("SharpCraft.Launcher", "SharpCraft Launcher");

        public static ProfileType BlizzardStarcraftIIEditor { get; } = new ProfileType("Blizzard.StarcraftII.Editor", "Starcraft II Editor");

        public static ProfileType BlizzardStarcraftIIGame { get; } = new ProfileType("Blizzard.StarcraftII.Game", "Starcraft II Game");

        public static ProfileType BlizzardWarcraftIIIEditor { get; } = new ProfileType("Blizzard.WarcraftIII.Editor", "Warcraft III Editor");

        public static ProfileType BlizzardWarcraftIIIGame { get; } = new ProfileType("Blizzard.WarcraftIII.Game", "Warcraft III Game");


        public static bool operator ==(ProfileType left, ProfileType right) => left.Equals(right);

        public static bool operator !=(ProfileType left, ProfileType right) => !left.Equals(right);

        public ProfileType(string name, string friendlyName = null)
        {
            this.Name = name;
            this.FriendlyName = friendlyName ?? name;
        }

        public string Name { get; }

        public string FriendlyName { get; }

        public bool Equals(ProfileType other) => string.Equals(this.Name, other.Name);

        public override bool Equals(object other)
        {
            if (ReferenceEquals(null, other))
                return false;
            return other is ProfileType && this.Equals((ProfileType)other);
        }

        public override int GetHashCode() => this.Name?.GetHashCode() ?? 0;
    }
}
