﻿namespace SharpCraft.Framework
{
    public interface IProfileLauncher
    {
        bool Launch(Profile profile, string arguments = null);
    }
}