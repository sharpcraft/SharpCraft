﻿using System.Collections.Generic;

namespace SharpCraft.Framework
{
    public class PluginInfo : IPluginInfo
    {
        public PluginInfo(string name, string description, string version, params string[] authors)
        {
            this.Name = name;
            this.Description = description;
            this.Version = version;
            this.Authors = new List<string>(authors);
        }

        public string Name { get;  }

        public string Description { get;  }

        public string Version { get;  }

        public IEnumerable<string> Authors { get;  }
    }
}
