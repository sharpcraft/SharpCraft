﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Windows;
using SharpCraft.Framework;
using SharpCraft.Launcher.ViewModels;
using SharpCraft.Launcher.ViewModels.Windows.Dialogs;
using SharpCraft.Launcher.Views.Windows.Dialogs;

namespace SharpCraft.Launcher
{
    [Export(typeof(IDialogService))]
    internal class WpfDialogService : IDialogService
    {
        public Profile CreateProfile(string profilesFolder, IEnumerable<Profile> profiles, Profile copyProfile = null, Window owner = null)
        {
            var createProfile = new CreateProfileViewModel(copyProfile?.Name, copyProfile?.Type, profiles);

            var dialog = new CreateProfileDialog
            {
                Owner = owner,
                WindowStartupLocation = WindowStartupLocation.CenterOwner,
                DataContext = createProfile
            };

            return dialog.ShowDialog() == true ? createProfile.CreateProfile(profilesFolder) : null;
        }

        public void ModifyProfile(ProfileViewModel profile, Window owner = null)
        {
            var dialog = new ProfileDialog
            {
                Owner = owner,
                WindowStartupLocation = WindowStartupLocation.CenterOwner,
                DataContext = profile
            };
            dialog.ShowDialog();
        }

        public MessageBoxResult Show(string message, string caption, MessageBoxButton buttons) => MessageBox.Show(message, caption, buttons);
    }
}
