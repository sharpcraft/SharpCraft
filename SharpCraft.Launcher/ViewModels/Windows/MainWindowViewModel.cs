﻿using System;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows;
using JetBrains.Annotations;
using Serilog;
using SharpCraft.Framework;

namespace SharpCraft.Launcher.ViewModels.Windows
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        private string statusText = "Welcome to SharpCraft!";

        [Export]
        private readonly SharpCraftEnvironment environment;

        private SharpCraftSettingsViewModel settings;

        public event PropertyChangedEventHandler PropertyChanged;

        public MainWindowViewModel()
        {
            if (VisualStudio.IsInDesignMode())
                return;

            this.environment = new SharpCraftEnvironment("SharpCraft.Launcher", Environment.CurrentDirectory);
            this.environment.LoadProfiles();

            Log.Logger?.Information("-------------------");
            Log.Logger?.Information("Composing MEF Graph for MainWindowViewModel");
            try
            {
                var catalog = new AssemblyCatalog(Assembly.GetExecutingAssembly());
                var container = new CompositionContainer(catalog);
                container.ComposeParts(this);
            }
            catch (ChangeRejectedException e)
            {
                var message = new StringBuilder();

                foreach (var item in e.Errors)
                    message.AppendLine(item.ToString());

                MessageBox.Show(message + Environment.NewLine + "The process will now terminate.", nameof(ChangeRejectedException), MessageBoxButton.OK, MessageBoxImage.Error);
                Process.GetCurrentProcess().Kill();
            }
            catch (Exception e)
            {
                MessageBox.Show(e + Environment.NewLine + "The process will now terminate.", e.GetType().Name, MessageBoxButton.OK, MessageBoxImage.Error);
                Process.GetCurrentProcess().Kill();
            }
        }

        [Import]
        public SharpCraftSettingsViewModel Settings
        {
            get { return this.settings; }
            set
            {
                if (Equals(value, this.settings))
                    return;
                this.settings = value;
                this.OnPropertyChanged();
            }
        }

        public string StatusText
        {
            get { return this.statusText; }
            set
            {
                if (value == this.statusText)
                    return;
                this.statusText = value;
                this.OnPropertyChanged();
            }
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
