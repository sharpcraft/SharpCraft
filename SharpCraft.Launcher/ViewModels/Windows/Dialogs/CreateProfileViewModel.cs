﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using JetBrains.Annotations;
using SharpCraft.Framework;

namespace SharpCraft.Launcher.ViewModels.Windows.Dialogs
{
    public class CreateProfileViewModel : INotifyPropertyChanged
    {
        private readonly IEnumerable<Profile> profiles;

        private string name;

        private ProfileType type;

        private bool canCreateProfile;

        private bool hasErrors;

        private string error;

        public event PropertyChangedEventHandler PropertyChanged;

        public CreateProfileViewModel() { }

        public CreateProfileViewModel(string name, ProfileType? type, IEnumerable<Profile> profiles)
        {
            this.profiles = profiles;
            this.Name = name ?? String.Empty;
            this.Type = type ?? ProfileType.BlizzardStarcraftIIEditor;
        }

        public string Name
        {
            get { return this.name; }
            set
            {
                if (value == this.name)
                    return;
                this.name = value ?? String.Empty;
                this.OnPropertyChanged();
                this.Validate();
            }
        }

        public ProfileType Type
        {
            get { return this.type; }
            set
            {
                if (value.Equals(this.type))
                    return;
                this.type = value;
                this.OnPropertyChanged();
                this.Validate();
            }
        }

        public bool CanCreateProfile
        {
            get { return this.canCreateProfile; }
            private set
            {
                if (value == this.canCreateProfile)
                    return;
                this.canCreateProfile = value;
                this.OnPropertyChanged();
            }
        }

        public bool HasErrors
        {
            get { return this.hasErrors; }
            private set
            {
                if (value == this.hasErrors)
                    return;
                this.hasErrors = value;
                this.OnPropertyChanged();
            }
        }

        public string Error
        {
            get { return this.error; }
            private set
            {
                if (value == this.error)
                    return;
                this.error = value;
                this.OnPropertyChanged();
            }
        }

        private void Validate()
        {
            this.CanCreateProfile = true;
            this.HasErrors = false;
            this.Error = String.Empty;
            if (String.IsNullOrEmpty(this.Name))
            {
                this.Error = "A profile name cannot be empty.";
                this.CanCreateProfile = false;
                this.HasErrors = true;
            }
            if (this.profiles.Any(p => p.Name == this.name))
            {
                this.Error = "A profile with this name already exist.";
                this.CanCreateProfile = false;
                this.HasErrors = true;
            }
            if (this.Name.IndexOfAny(Path.GetInvalidFileNameChars()) >= 0)
            {
                this.Error = "A profile name must be a valid folder name.";
                this.CanCreateProfile = false;
                this.HasErrors = true;
            }
        }

        public Profile CreateProfile(string profilesDirectory) => new Profile(Path.Combine(profilesDirectory, this.Name), this.type);

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
