﻿using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.CompilerServices;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using JetBrains.Annotations;
using Microsoft.Win32;
using SharpCraft.Framework;

namespace SharpCraft.Launcher.ViewModels
{
    public class ProfileViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private readonly SharpCraftSettingsViewModel settings;

        private bool isIncomplete;

        public ProfileViewModel(SharpCraftSettingsViewModel settings, Profile profile)
        {
            this.settings = settings;
            if (profile == null)
                throw new ArgumentNullException(nameof(profile));
            this.Profile = profile;
            this.LaunchCommand = new RelayCommand(p => new ProfileLauncher().Launch(this.Profile));
            this.RemoveCommand = new RelayCommand(p => this.Remove(), p => this.settings != null);
            this.ModifyCommand = new RelayCommand(p => this.Modify(), p => this.settings != null);
            this.BrowseFileNameCommand = new RelayCommand(p => this.BrowseFileName());
            this.BrowseWorkingDirectoryCommand = new RelayCommand(p => this.BrowseWorkingDirectory());
            if (File.Exists(Path.Combine(this.Profile.Root, "icon.png")))
            {
                var path = Path.Combine(this.Profile.Root, "icon.png");

                var image = new BitmapImage();
                image.BeginInit();
                image.StreamSource = File.Open(path, FileMode.Open, FileAccess.Read, FileShare.Read);
                image.EndInit();
                this.Icon = image;
            }
            this.Validate();
        }

        public Profile Profile { get; }

        public ImageSource Icon { get; }

        public RelayCommand LaunchCommand { get; }

        public RelayCommand RemoveCommand { get; }

        public RelayCommand ModifyCommand { get; }

        public RelayCommand BrowseFileNameCommand { get; }

        public RelayCommand BrowseWorkingDirectoryCommand { get; }

        public string Name => this.Profile.Name;

        public ProfileType Type
        {
            get { return this.Profile.Type; }
            set
            {
                if (value == this.Profile.Type)
                    return;
                this.Profile.Type = value;
                this.OnPropertyChanged();
            }
        }

        public string Directory => this.Profile.Root;

        public string FileName
        {
            get { return this.Profile.FileName; }
            set
            {
                if (value == this.Profile.FileName)
                    return;
                this.Profile.FileName = value;
                this.OnPropertyChanged();
                this.Validate();
            }
        }

        public string Arguments
        {
            get { return this.Profile.Arguments; }
            set
            {
                if (value == this.Profile.Arguments)
                    return;
                this.Profile.Arguments = value;
                this.OnPropertyChanged();
                this.Validate();
            }
        }

        public string WorkingDirectory
        {
            get { return this.Profile.WorkingDirectory; }
            set
            {
                if (value == this.Profile.WorkingDirectory)
                    return;
                this.Profile.WorkingDirectory = value;
                this.OnPropertyChanged();
                this.Validate();
            }
        }

        public bool EnableSharpCraft
        {
            get { return this.Profile.EnableSharpCraft; }
            set
            {
                if (value == this.Profile.EnableSharpCraft)
                    return;
                this.Profile.EnableSharpCraft = value;
                this.OnPropertyChanged();
                this.Validate();
            }
        }

        public bool EnableDebug
        {
            get { return this.Profile.EnableDebug; }
            set
            {
                if (value == this.Profile.EnableDebug)
                    return;
                this.Profile.EnableDebug = value;
                this.OnPropertyChanged();
                this.Validate();
            }
        }

        public bool IsIncomplete
        {
            get { return this.isIncomplete; }
            private set
            {
                if (value == this.isIncomplete)
                    return;
                this.isIncomplete = value;
                this.OnPropertyChanged();
            }
        }

        public void Remove() => this.settings.RemoveProfile(this);

        public void Modify() => this.settings.ModifyProfile(this);

        public void BrowseFileName()
        {
            var dialog = new OpenFileDialog
            {
                Filter = "All Files|*.*|Starcraft II - Editor|sc2editor.exe|Starcraft II - Game|sc2.exe|Warcraft III - Editor|worldedit.exe;World Editor.exe|Warcraft III - Game|war3.exe;Warcraft III.exe"
            };

            if (this.Type == ProfileType.BlizzardStarcraftIIEditor)
                dialog.FilterIndex = 2;
            else if (this.Type == ProfileType.BlizzardStarcraftIIGame)
                dialog.FilterIndex = 3;
            else if (this.Type == ProfileType.BlizzardWarcraftIIIEditor)
                dialog.FilterIndex = 4;
            else if (this.Type == ProfileType.BlizzardWarcraftIIIGame)
                dialog.FilterIndex = 5;
            if (dialog.ShowDialog() == true)
            {
                if (!this.EnableSharpCraft)
                {
                    // can't support different working directory without SharpCraft being enabled
                    this.FileName = dialog.FileName;
                    this.WorkingDirectory = Path.GetDirectoryName(this.FileName);
                }
                else
                {
                    var oldFileName = this.FileName;
                    this.FileName = dialog.FileName;
                    if (oldFileName.StartsWith(this.WorkingDirectory))
                        this.WorkingDirectory = Path.GetDirectoryName(this.FileName);
                }
            }
        }

        public void BrowseWorkingDirectory() { }

        public void Validate() { }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
