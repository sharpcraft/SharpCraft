﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.IO;
using System.Runtime.CompilerServices;
using System.Windows;
using JetBrains.Annotations;
using SharpCraft.Framework;

namespace SharpCraft.Launcher.ViewModels
{
    [Export]
    public class SharpCraftSettingsViewModel : INotifyPropertyChanged, IPartImportsSatisfiedNotification
    {
        public event PropertyChangedEventHandler PropertyChanged;

        [Import]
        private SharpCraftEnvironment environment;

        public SharpCraftSettingsViewModel()
        {
            this.CreateProfileCommand = new RelayCommand(p => this.CreateProfile());
        }

        public ObservableCollection<ProfileViewModel> Profiles { get; } = new ObservableCollection<ProfileViewModel>();

        public RelayCommand CreateProfileCommand { get; }

        public void CreateProfile()
        {
            var profile = Services.Instance.Dialog.CreateProfile(this.environment.ProfilesDirectory, this.environment.Profiles, null, Application.Current.MainWindow);
            if (profile != null)
            {
                var profileVM = new ProfileViewModel(this, profile);
                this.AddProfile(profileVM);
                this.ModifyProfile(profileVM);
            }
        }

        public void ModifyProfile(ProfileViewModel profile)
        {
            if (profile == null)
                return;
            Services.Instance.Dialog.ModifyProfile(profile, Application.Current.MainWindow);
            profile.Profile.Save();
        }

        public void AddProfile(ProfileViewModel profile)
        {
            if (profile == null)
                return;
            this.Profiles.Add(profile);
            this.environment.Profiles.Add(profile.Profile);
            profile.Profile.Save();
        }

        public void RemoveProfile(ProfileViewModel profile)
        {
            if (profile == null)
                return;
            this.Profiles.Remove(profile);
            this.environment.Profiles.Remove(profile.Profile);

            if (Directory.Exists(profile.Profile.Root))
                Directory.Delete(profile.Profile.Root, true);
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public void OnImportsSatisfied()
        {
            this.Profiles.Clear();
            foreach (var profile in this.environment.Profiles)
                this.Profiles.Add(new ProfileViewModel(this, profile));
        }
    }
}
