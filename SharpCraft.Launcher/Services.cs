﻿using System;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Diagnostics;
using System.Reflection;
using System.Text;
using System.Windows;
using Serilog;

namespace SharpCraft.Launcher
{
    public class Services
    {
        static Services()
        {
            Log.Logger?.Information("-------------------");
            Log.Logger?.Information("Composing MEF Graph for Services");
            try
            {
                Instance = new Services();
                var catalog = new AssemblyCatalog(Assembly.GetExecutingAssembly());
                var container = new CompositionContainer(catalog);
                container.ComposeParts(Instance);
            }
            catch (ChangeRejectedException e)
            {
                var message = new StringBuilder();

                foreach (var item in e.Errors)
                    message.AppendLine(item.ToString());

                MessageBox.Show(message + Environment.NewLine + "The process will now terminate.", nameof(ChangeRejectedException), MessageBoxButton.OK, MessageBoxImage.Error);
                Process.GetCurrentProcess().Kill();
            }
            catch (Exception e)
            {
                MessageBox.Show(e + Environment.NewLine + "The process will now terminate.", e.GetType().Name, MessageBoxButton.OK, MessageBoxImage.Error);
                Process.GetCurrentProcess().Kill();
            }
        }

        public static Services Instance { get; }


        private Services() { }

        [Import]
        public IDialogService Dialog { get; private set; }
    }
}
