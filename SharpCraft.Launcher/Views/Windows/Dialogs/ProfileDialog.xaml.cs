﻿using SharpCraft.Framework;

namespace SharpCraft.Launcher.Views.Windows.Dialogs
{
    public partial class ProfileDialog
    {
        public ProfileDialog()
        {
            this.InitializeComponent();

            this.TypeComboBox.Items.Add(ProfileType.BlizzardStarcraftIIEditor);
            this.TypeComboBox.Items.Add(ProfileType.BlizzardStarcraftIIGame);
            this.TypeComboBox.Items.Add(ProfileType.BlizzardWarcraftIIIEditor);
            this.TypeComboBox.Items.Add(ProfileType.BlizzardWarcraftIIIGame);
        }
    }
}
