﻿using System.Windows;
using SharpCraft.Framework;

namespace SharpCraft.Launcher.Views.Windows.Dialogs
{
    public partial class CreateProfileDialog
    {
        public CreateProfileDialog()
        {
            this.InitializeComponent();
            this.ComboBox.Items.Add(ProfileType.BlizzardStarcraftIIEditor);
            this.ComboBox.Items.Add(ProfileType.BlizzardStarcraftIIGame);
            this.ComboBox.Items.Add(ProfileType.BlizzardWarcraftIIIEditor);
            this.ComboBox.Items.Add(ProfileType.BlizzardWarcraftIIIGame);
        }

        private void OKButton_OnClick(object sender, RoutedEventArgs e) => this.DialogResult = true;
    }
}
