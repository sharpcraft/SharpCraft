﻿using System.Windows.Input;

namespace SharpCraft.Launcher.Views.Windows
{
    public partial class MainWindow
    {
        public MainWindow()
        {
            this.InitializeComponent();
        }

        private void CommandBinding_OnExecuted(object sender, ExecutedRoutedEventArgs e) => this.Close();
    }
}
