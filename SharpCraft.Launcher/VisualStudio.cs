﻿using System.Reflection;

namespace SharpCraft.Launcher
{
    public static class VisualStudio
    {
        public static bool IsInDesignMode() => Assembly.GetExecutingAssembly().Location?.Contains("VisualStudio") ?? false;
    }
}
