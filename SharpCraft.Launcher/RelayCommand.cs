﻿using System;
using System.Windows.Input;

namespace SharpCraft.Launcher
{
    public class RelayCommand : ICommand
    {
        private readonly Action<object> executeAction;

        private readonly Predicate<object> canExecutePredicate;

        public event EventHandler CanExecuteChanged;

        public RelayCommand(Action<object> executeAction, Predicate<object> canExecutePredicate = null)
        {
            if (executeAction == null)
                throw new ArgumentNullException(nameof(executeAction));
            this.executeAction = executeAction;
            this.canExecutePredicate = canExecutePredicate;
        }

        public bool CanExecute(object parameter) => this.canExecutePredicate?.Invoke(parameter) ?? true;

        public void Execute(object parameter) => this.executeAction.Invoke(parameter);

        public void NotifyCanExecuteChanged(EventArgs e) => this.CanExecuteChanged?.Invoke(this, e);

        public void NotifyCanExecuteChanged() => this.NotifyCanExecuteChanged(EventArgs.Empty);
    }
}
