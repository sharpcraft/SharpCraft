﻿using System.Collections.Generic;
using System.Windows;
using SharpCraft.Framework;
using SharpCraft.Launcher.ViewModels;

namespace SharpCraft.Launcher
{
    public interface IDialogService
    {
        Profile CreateProfile(string profilesFolder, IEnumerable<Profile> profiles, Profile copyProfile = null, Window owner = null);
        void ModifyProfile(ProfileViewModel profile, Window owner = null);
        MessageBoxResult Show(string message, string caption, MessageBoxButton buttons);
    }
}
