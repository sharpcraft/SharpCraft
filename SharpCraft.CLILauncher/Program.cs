﻿using System;
using System.IO;
using System.Linq;
using Serilog;
using SharpCraft.Framework;

namespace SharpCraft.CLILauncher
{
    public class Program
    {
        [STAThread]
        public static void Main(string[] args)
        {
            Environment.CurrentDirectory = Path.GetDirectoryName(typeof(EntryPoint).Assembly.Location);

            var environment = new SharpCraftEnvironment("SharpCraft.CLILauncher", Environment.CurrentDirectory);
            environment.LoadProfiles();

            if (args.Length == 0)
            {
                Log.Logger.Error("Invalid arguments.");
                Log.Logger.Error("Usage: SharpCraft.CLILauncher.exe \"<profile name>\" <additional optional arguments>");
                return;
            }

            var profileName = args[0].Trim('"');
            Console.WriteLine(profileName);
            var profile = environment.Profiles.FirstOrDefault(p => p.Name == profileName);
            if (profile == null)
            {
                Log.Logger.Error("Invalid arguments.");
                Log.Logger.Error("\"{profileName}\" not found.", profileName);
                return;
            }

            var profileLauncher = new ProfileLauncher();
            var arguments = args.Skip(1).Select(arg => arg.Any(Char.IsWhiteSpace) ? '"' + arg + '"' : arg).ToList();
            if (arguments.Any())
                profileLauncher.Launch(profile, arguments.Aggregate((a, b) => a + " " + b));
            else
                profileLauncher.Launch(profile);
        }
    }
}
