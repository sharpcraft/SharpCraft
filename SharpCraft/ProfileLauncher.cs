﻿using System;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows;
using EasyHook;
using Microsoft.Win32;
using MindWorX.Unmanaged.Windows;
using SharpCraft.Framework;

namespace SharpCraft
{
    [Export(typeof(IProfileLauncher))]
    public unsafe class ProfileLauncher : IProfileLauncher
    {
        private static bool TryToLocate(ProfileType profileType, out string path)
        {
            path = null;

            if (profileType == ProfileType.BlizzardWarcraftIIIEditor || profileType == ProfileType.BlizzardWarcraftIIIGame)
            {
                path = Registry.CurrentUser.OpenSubKey("Software\\Blizzard Entertainment\\Warcraft III\\")?.GetValue("InstallPath") as string;
                return path != null;
            }

            return false;
        }

        public bool Launch(Profile profile, string arguments = null)
        {
            var launch = true;
            var thread = new Thread(() =>
            {
                if (String.IsNullOrEmpty(profile.FileName) || !File.Exists(profile.FileName))
                {
                    var message = new StringBuilder();
                    message.AppendLine("Profile not configured correctly.");
                    message.Append("You need to configure the path to ");
                    if (profile.Type == ProfileType.BlizzardStarcraftIIEditor)
                        message.AppendLine("SC2Editor.exe.");
                    else if (profile.Type == ProfileType.BlizzardStarcraftIIGame)
                        message.AppendLine("SC2.exe.");
                    else if (profile.Type == ProfileType.BlizzardWarcraftIIIEditor)
                        message.AppendLine("WorldEdit.exe(1.28.2 and below) or World Editor.exe(1.28.3 and above)");
                    else if (profile.Type == ProfileType.BlizzardWarcraftIIIGame)
                        message.AppendLine("War3.exe(1.28.2 and below) or Warcraft III.exe(1.28.3 and above)");
                    else
                        message.AppendLine(profile.Type.FriendlyName);
                    message.AppendLine("SharpCraft will attempt to locate it automatically.");
                    message.AppendLine();
                    message.AppendLine("Configure profile now?");

                    if (MessageBox.Show(message.ToString(), "Incomplete Profile", MessageBoxButton.YesNo) != MessageBoxResult.Yes)
                    {
                        launch = false;
                        return;
                    }

                    var dialog = new OpenFileDialog
                    {
                        Filter = "All Files|*.*|Starcraft II - Editor|sc2editor.exe|Starcraft II - Game|sc2.exe|Warcraft III - Editor|worldedit.exe;World Editor.exe|Warcraft III - Game|war3.exe;Warcraft III.exe",
                    };

                    string path;
                    if (TryToLocate(profile.Type, out path))
                        dialog.InitialDirectory = path;

                    if (profile.Type == ProfileType.BlizzardStarcraftIIEditor)
                        dialog.FilterIndex = 2;
                    else if (profile.Type == ProfileType.BlizzardStarcraftIIGame)
                        dialog.FilterIndex = 3;
                    else if (profile.Type == ProfileType.BlizzardWarcraftIIIEditor)
                        dialog.FilterIndex = 4;
                    else if (profile.Type == ProfileType.BlizzardWarcraftIIIGame)
                        dialog.FilterIndex = 5;

                    if (dialog.ShowDialog() == true)
                    {
                        profile.FileName = dialog.FileName;
                        profile.WorkingDirectory = Path.GetDirectoryName(profile.FileName);
                        profile.Save();
                    }
                    else
                    {
                        launch = false;
                    }
                }
            });
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
            thread.Join();

            profile.Arguments = profile.Arguments?.Replace("%PROFILE_FOLDER%", profile.Root);

            return launch && (profile.EnableSharpCraft ? this.LaunchSharpCraft(profile, arguments) : this.LaunchVanilla(profile, arguments));
        }

        public bool LaunchSharpCraft(string root, ProfileType profileType, string arguments = null, bool enableDebug = false, bool enableDiagnosticsDebug = false)
        {
            Profile profile;
            try
            {
                profile = Profile.Load(root);
            }
            catch (FileNotFoundException)
            {
                profile = new Profile(root, profileType)
                {
                    EnableSharpCraft = true,
                    EnableDebug = enableDebug,
                    EnableDiagnosticsDebug = enableDiagnosticsDebug
                };
            }

            return this.Launch(profile, arguments);
        }

        private bool LaunchVanilla(Profile profile, string arguments)
        {
            if (!String.IsNullOrEmpty(arguments))
                arguments = " " + arguments;
            var processStartInfo = new ProcessStartInfo
            {
                FileName = profile.FileName,
                Arguments = profile.Arguments + arguments,
                WorkingDirectory = profile.WorkingDirectory
            };
            return Process.Start(processStartInfo) != null;
        }

        private bool LaunchSharpCraft(Profile profile, string arguments)
        {
            if (!String.IsNullOrEmpty(arguments))
                arguments = " " + arguments;
            var startupInfo = new STARTUPINFO();
            PROCESS_INFORMATION processInformation;
            if (!Kernel32.CreateProcessA(profile.FileName, "\"" + profile.FileName + "\" " + profile.Arguments + arguments, null, null, false, ProcessCreationFlags.CREATE_SUSPENDED, null, profile.WorkingDirectory, ref startupInfo, out processInformation))
                return false;

            RemoteHooking.Inject((int)processInformation.dwProcessId, "SharpCraft.dll", "SharpCraft.dll", processInformation.dwThreadId, profile);
            return true;
        }
    }
}
