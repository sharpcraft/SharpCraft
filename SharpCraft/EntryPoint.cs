﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows;
using EasyHook;
using MindWorX.Unmanaged;
using MindWorX.Unmanaged.Windows;
using Serilog;
using SharpCraft.Framework;

namespace SharpCraft
{
    public class EntryPoint : IEntryPoint
    {
        [Export]
        private readonly Profile profile;

        [Export]
        private readonly SharpCraftEnvironment environment;

        [ImportMany(typeof(InitializeDelegate))]
        private List<InitializeDelegate> initializeDelegates;

        [ImportMany(typeof(IPlugin))]
        private List<IPlugin> plugins;

        private ComposablePartCatalog catalog;

        private CompositionContainer container;

        private LocalHook getModuleFileNameALocalHook;

        public EntryPoint(RemoteHooking.IContext hookingContext, uint threadId, Profile profile)
        {
            AppDomain.CurrentDomain.AssemblyLoad += (sender, args) =>
            {
                Log.Information("Loaded {@moduleName}", args.LoadedAssembly.FullName);
            };
            AppDomain.CurrentDomain.AssemblyResolve += (sender, args) =>
            {
                Log.Error("Unable to locate {@moduleName}", args.Name);

                return null;
            };

            Environment.CurrentDirectory = Path.GetDirectoryName(typeof(EntryPoint).Assembly.Location);

            this.environment = new SharpCraftEnvironment("SharpCraft", Environment.CurrentDirectory);
            this.profile = profile;
        }

        public void Run(RemoteHooking.IContext hookingContext, uint threadId, Profile profile)
        {
            if (this.profile?.EnableDiagnosticsDebug == true)
            {
                Log.Information("Diagnostics debugging enabled.");
                Debugger.Launch();
            }

            if (this.profile?.EnableDebug == true)
            {
                Log.Information("Debugging enabled.");
                Kernel32.AllocConsole();
                Console.SetOut(new StreamWriter(Console.OpenStandardOutput()) { AutoFlush = true });
            }
            Log.Information("-------------------");
            Log.Information("-- Profile: {profileName}", this.profile?.Name ?? "N/A");

            Log.Information("Injecting GetModuleFileName hook . . .");
            if (!String.IsNullOrEmpty(this.profile?.WorkingDirectory) && !String.IsNullOrEmpty(Path.GetFileName(this.profile?.FileName)))
            {
                this.getModuleFileNameALocalHook = LocalHook.Create(Kernel32.GetProcAddress(Kernel32.Handle, nameof(Kernel32.GetModuleFileNameA)), new Kernel32.GetModuleFileNameAPrototype(this.GetModuleFileNameAHook), null);
                this.getModuleFileNameALocalHook.ThreadACL.SetExclusiveACL(new[] { 0 });
            }

            Log.Information("Composing MEF Graph . . .");
            try
            {
                var directories = new HashSet<string>();
                directories.Add(this.environment.Root);
                directories.Add(this.environment.PluginsDirectory);
                foreach (var directory in Directory.EnumerateDirectories(this.environment.PluginsDirectory, "*.*", SearchOption.AllDirectories))
                    directories.Add(directory);
                if (this.profile != null)
                {
                    directories.Add(this.profile.PluginsDirectory);
                    foreach (var directory in Directory.EnumerateDirectories(this.profile.PluginsDirectory, "*.*", SearchOption.AllDirectories))
                        directories.Add(directory);
                }
                var catalogs = new List<ComposablePartCatalog> { new AssemblyCatalog(typeof(EntryPoint).Assembly) };
                foreach (var directory in directories)
                {
                    Log.Information("  Directory {@path}", new { Path = directory });
                    catalogs.Add(new DirectoryCatalog(directory));
                }

                this.catalog = new AggregateCatalog(catalogs);
                this.container = new CompositionContainer(this.catalog);
                this.container.ComposeParts(this);
            }
            catch (ChangeRejectedException e)
            {
                var message = new StringBuilder();

                foreach (var item in e.Errors)
                    message.AppendLine(item.ToString());

                MessageBox.Show(message + Environment.NewLine + "The process will now terminate.", nameof(ChangeRejectedException), MessageBoxButton.OK, MessageBoxImage.Error);
                Process.GetCurrentProcess().Kill();
            }
            catch (Exception e)
            {
                MessageBox.Show(e + Environment.NewLine + "The process will now terminate.", e.GetType().Name, MessageBoxButton.OK, MessageBoxImage.Error);
                Process.GetCurrentProcess().Kill();
            }

            foreach (var initializeDelegate in this.initializeDelegates)
            {
                try
                {
                    initializeDelegate();
                }
                catch (Exception e)
                {
                    Log.Error(e.ToString());
                }
            }

            foreach (var plugin in this.plugins)
            {
                if (plugin.Info == null)
                {
                    Log.Error("Skipping {@pluginTypeName} because info is missing.", new { PluginTypeName = plugin.GetType().FullName });
                    continue;
                }
                Log.Information("Initializing {@plugin}", new { PluginName = plugin.Info.Name, PluginVersion = plugin.Info.Version, ProfileType = this.profile?.TypeValue });
                try
                {
                    plugin.Initialize();
                }
                catch (Exception e)
                {
                    Log.Error(e.ToString());
                }
            }

            // Everyone has had their chance to inject stuff,
            // time to wake up the process.
            if (threadId != 0)
            {
                using (var thread = Kernel32.OpenDisposableThread(ThreadAccess.SUSPEND_RESUME, false, threadId))
                    Kernel32.ResumeThread(thread.Handle);
            }

            // Let the thread stay alive, so all hooks stay alive as well.
            // This might need to be shutdown properly on exit.
            Thread.Sleep(Timeout.Infinite);
        }

        private uint GetModuleFileNameAHook(IntPtr hModule, IntPtr lpFilename, uint nSize)
        {
            var result = Kernel32.GetModuleFileNameA(hModule, lpFilename, nSize);
            var fileName = Marshal.PtrToStringAnsi(lpFilename, (int)result);

            if (Path.GetFileName(fileName) != Path.GetFileName(this.profile.FileName))
                return result;

            var path = Path.Combine(this.profile.WorkingDirectory, Path.GetFileName(this.profile.FileName));
            Memory.WriteString(lpFilename, path);
            return (uint)path.Length;
        }
    }
}
